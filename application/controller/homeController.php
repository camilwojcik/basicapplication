<?php

class homeController extends Controller
{

    public function indexAction() : void
    {
        $_home = Home::getInstance();

        $params = $_POST['element'] ?? 0;
        if(!empty($params['name']) && !empty($params['type'])){

            $params = $_POST['element'];
            $_home->addRecord($params['name'], $params['type']);
        }

        $this->view($this->getSourcePath(__CLASS__ , __FUNCTION__), [
            'elements' => $_home->getElements(),
            'statistics' => $_home->getStatistics()
        ]);

        $this->view->page_title = 'Books and so on';
        $this->view->renderView();
    }

    public function deleteAction() : void
    {
        if(!empty($_POST['nuke'])){
            $_home = Home::getInstance();
            $nuked_records = $_home->removeElements();
        }

        $this->view($this->getSourcePath(__CLASS__ , __FUNCTION__), [
        ]);

        $this->view->nuked_records = $nuked_records ?? 0;
        $this->view->page_title = 'Nuke page';
        $this->view->renderView();
    }

}