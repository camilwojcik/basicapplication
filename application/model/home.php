<?php


final class Home extends BaseModel
{
    /**
     * @var Singleton
     */
    private static $instance;

    /**
     * @return Home|Singleton
     */
    public static function getInstance() : Home
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**Returns all elements from table
     * @return array assoc array of records from table "elements"
     */
    public function getElements() : array
    {
        $arr = [];
        $sql = $this->_db->prepare("SELECT * FROM elements AS e WHERE e.active=1");
        $sql->execute([]);
        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $row['additional_info'] = json_decode($row['additional_info'], true);
            $type = $this->getElementType($row['additional_info']);

            switch ($type){
                case ELEMENT_BOOK:
                    $arr['books'][] = $row;
                    break;
                case ELEMENT_NEWSPAPER:
                    $arr['newspapers'][] = $row;
                    break;
                case ELEMENT_POSTER:
                    $arr['posters'][] = $row;
                    break;
                default:
                    $arr['elements'][] = $row;
                    break;
            }
        }
        unset($sql);
        return $arr;
    }

    /**Return element type despite of it`s additional info
     * @param array $additional_info - decoded JSON string from db table `elements`
     * @return string - const ELEMENT_* based on param
     */
    public function getElementType(array $additional_info) : string
    {
        if(!empty($additional_info['orig_lang']) && $additional_info['lang'])
            return ELEMENT_BOOK;
        if(!empty($additional_info['number']))
            return ELEMENT_NEWSPAPER;
        return ELEMENT_POSTER;
    }

    /**Creates a records of a element (with random data)
     * @param string $name - name of our element
     * @param string $type - type of element (See in consts ELEMENTS)
     * @return bool - true on success
     */
    public function addRecord(string $name, string $type = ELEMENT_BOOK) : bool
    {
        $data = $this->getRandomElementData($type);
        $data['name'] = $name;

        $sql = "INSERT INTO elements (name, volume, copies, release_year, additional_info, active) VALUES (:name, :volume, :copies, :release_year, :additional_info, 1)";
        $this->_db->prepare($sql)->execute($data);
        unset($sql);

        $this->updateStatistics();

        return true;
    }

    /**Generates random data based on given ELEMENT_TYPE
     * @param string $type ELEMENT_*
     * @return array data structured as table row
     */
    public function getRandomElementData(string $type) : array
    {
        $additional_info = [];
        switch ($type){
            case ELEMENT_BOOK:
                $languages = $this->getDictionaryValues('languages');
                $additional_info[ADDITIONAL_INFO_LANGUAGE] = $languages[rand(0, sizeof($languages)-1)]['value'];
                $additional_info[ADDITIONAL_INFO_ORIGINAL_LANGUAGE] = $languages[rand(0, sizeof($languages)-1)]['value'];

                $volume = rand(MIN_PAGE_COUNT, MAX_PAGE_COUNT);
                break;
            case ELEMENT_NEWSPAPER:
                $additional_info[ADDITIONAL_INFO_NUMBER] = rand(MIN_NUMBER, MAX_NUMBER);
                $volume = rand(MIN_PAGE_COUNT, MAX_PAGE_COUNT);
                break;
            case ELEMENT_POSTER:
                $volume = rand(MIN_POSTER_WIDTH, MAX_POSTER_WIDTH) . 'x' . rand(MIN_POSTER_HEIGHT, MAX_POSTER_HEIGHT);
                break;
            default:
                $volume = 0;
                break;
        }

        return [
            'volume' => $volume,
            'copies' => rand(MIN_COPIES, MAX_COPIES),
            'release_year' => rand(MIN_YEAR, (int)date('Y')),
            'additional_info' => json_encode($additional_info, true)
        ];
    }

    /**selects active row from statistics
     * @return array
     */
    public function getStatistics():array
    {
        $sql = $this->_db->prepare("SELECT * FROM statistics AS s WHERE s.active=1 ORDER BY s.id DESC LIMIT 1");
        $sql->execute([]);
        return $sql->fetch(PDO::FETCH_ASSOC);
    }

    /**Deletion by deactivation to keep history of our elements
     * @return int - count of affected rows
     */
    public function removeElements(): int
    {
        $sql = $this->_db->prepare("UPDATE elements SET active=0 WHERE active=1");
        $sql->execute([]);
        $this->updateStatistics();
        return $sql->rowCount();
    }

    /**Updates table statistics with fresh data
     * @noinspection SqlWithoutWhere */
    private function updateStatistics():void
    {
        $sql = $this->_db->prepare("UPDATE statistics SET active=0");
        $sql->execute([]);
        $groupStats = $this->getGroupStats();

        $sql = $this->_db->prepare("INSERT INTO statistics (unique_elements, copies, books, newspapers, posters) 
            VALUE (:unique, :copies, :books, :newspapers, :posters)");
        $sql->execute([
            'unique' => $this->getUniqueStat(),
            'copies' =>$this->getCopiesStat(),
            'books' => $groupStats['books'],
            'newspapers' => $groupStats['newspapers'],
            'posters' => $groupStats['posters']
        ]);
    }

    /**Selects unique elements (based on name)
     * @return int
     */
    private function getUniqueStat():int
    {
        $sql = $this->_db->prepare("SELECT COUNT(DISTINCT name) as `unique_elements` FROM elements as e WHERE e.active=1");
        $sql->execute([]);
        return $sql->fetch(PDO::FETCH_ASSOC)['unique_elements'] ?? 0;
    }

    /**
     * @return int sum of all copies of elements (0 if no elements)
     */
    private function getCopiesStat(): int
    {
        $sql = $this->_db->prepare("SELECT SUM(e.copies) as `copies`  FROM elements as e WHERE e.active=1");
        $sql->execute([]);
        return $sql->fetch(PDO::FETCH_ASSOC)['copies'] ?? 0;
    }

    /**
     * @return array array if counted elements by TYPE
     */
    private function getGroupStats(): array
    {
        $elements = $this->getElements();
        return array(
            'books'=>@count($elements['books']) ?? 0,
            'newspapers' => @count($elements['newspapers'] )?? 0,
            'posters' => @count($elements['posters']) ?? 0
        );
    }
}