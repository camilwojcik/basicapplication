-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table recruitmentprojectv1.elements
CREATE TABLE IF NOT EXISTS `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(41) NOT NULL,
  `volume` int(11) NOT NULL DEFAULT '0',
  `quanity` int(11) NOT NULL DEFAULT '0',
  `publishment_year` int(11) NOT NULL,
  `additional_info` text,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table recruitmentprojectv1.elements: ~0 rows (approximately)
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` (`id`, `name`, `volume`, `quanity`, `publishment_year`, `additional_info`) VALUES
	(1, 'test', 22, 1, 1998, NULL);
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;

-- Dumping structure for table recruitmentprojectv1.statistics
CREATE TABLE IF NOT EXISTS `statistics` (
  `id` int(11) NOT NULL,
  `unique_elements` int(11) NOT NULL,
  `copies_sum` int(11) NOT NULL,
  `books_amount` int(11) NOT NULL,
  `newsletters_amount` int(11) NOT NULL,
  `posters_amount` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table recruitmentprojectv1.statistics: ~0 rows (approximately)
/*!40000 ALTER TABLE `statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `statistics` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
