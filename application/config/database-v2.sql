-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               10.1.38-MariaDB - mariadb.org binary distribution
-- Serwer OS:                    Win64
-- HeidiSQL Wersja:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Zrzut struktury bazy danych recruitmentprojectv1
CREATE DATABASE IF NOT EXISTS `recruitmentprojectv1` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci */;
USE `recruitmentprojectv1`;

-- Zrzut struktury tabela recruitmentprojectv1.elements
CREATE TABLE IF NOT EXISTS `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(41) COLLATE utf8_polish_ci NOT NULL,
  `volume` varchar(11) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `copies` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `additional_info` text COLLATE utf8_polish_ci,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Zrzucanie danych dla tabeli recruitmentprojectv1.elements: ~0 rows (około)
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` (`id`, `name`, `volume`, `copies`, `create_date`, `additional_info`) VALUES
	(1, 'ddd', '0', 0, '2019-12-09 18:03:25', NULL);
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;

-- Zrzut struktury tabela recruitmentprojectv1.statistics
CREATE TABLE IF NOT EXISTS `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_elements` int(11) NOT NULL DEFAULT '0',
  `copies` int(11) NOT NULL DEFAULT '0',
  `books` int(11) NOT NULL DEFAULT '0',
  `newsletters` int(11) NOT NULL DEFAULT '0',
  `posters` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Zrzucanie danych dla tabeli recruitmentprojectv1.statistics: ~0 rows (około)
/*!40000 ALTER TABLE `statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `statistics` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
