-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               10.1.38-MariaDB - mariadb.org binary distribution
-- Serwer OS:                    Win64
-- HeidiSQL Wersja:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Zrzut struktury bazy danych recruitmentprojectv1
CREATE DATABASE IF NOT EXISTS `recruitmentprojectv1` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci */;
USE `recruitmentprojectv1`;

-- Zrzut struktury tabela recruitmentprojectv1.dictionaries
CREATE TABLE IF NOT EXISTS `dictionaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(41) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `key` varchar(41) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Zrzucanie danych dla tabeli recruitmentprojectv1.dictionaries: ~0 rows (około)
/*!40000 ALTER TABLE `dictionaries` DISABLE KEYS */;
INSERT INTO `dictionaries` (`id`, `name`, `key`) VALUES
	(1, 'Languages', 'languages');
/*!40000 ALTER TABLE `dictionaries` ENABLE KEYS */;

-- Zrzut struktury tabela recruitmentprojectv1.dictionaries_values
CREATE TABLE IF NOT EXISTS `dictionaries_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(41) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `dictionary_id` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Zrzucanie danych dla tabeli recruitmentprojectv1.dictionaries_values: ~2 rows (około)
/*!40000 ALTER TABLE `dictionaries_values` DISABLE KEYS */;
INSERT INTO `dictionaries_values` (`id`, `value`, `dictionary_id`, `active`) VALUES
	(1, 'Polish', 1, 1),
	(2, 'English', 1, 1),
	(3, 'Romanian', 1, 1);
/*!40000 ALTER TABLE `dictionaries_values` ENABLE KEYS */;

-- Zrzut struktury tabela recruitmentprojectv1.elements
CREATE TABLE IF NOT EXISTS `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(41) COLLATE utf8_polish_ci NOT NULL,
  `volume` varchar(11) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `copies` int(11) NOT NULL DEFAULT '0',
  `release_year` int(4) NOT NULL,
  `additional_info` text COLLATE utf8_polish_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Zrzucanie danych dla tabeli recruitmentprojectv1.elements: ~39 rows (około)
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` (`id`, `name`, `volume`, `copies`, `release_year`, `additional_info`, `active`) VALUES
	(46, 'test', '26', 0, 1996, '{"lang":"Polish","orig_lang":"Romanian"}', 0),
	(47, 'test2', '38x58', 9, 2018, '{}', 0),
	(48, 'test3', '19x63', 7, 1990, '{}', 0),
	(49, 'test4', '374', 7, 1994, '{"number":6}', 0),
	(50, 'test5', '35', 7, 2004, '{"number":5}', 0),
	(51, 'test6', '221', 3, 1990, '{"lang":"Romanian","orig_lang":"Romanian"}', 0),
	(52, 'asdasd', '55', 2, 2004, '{"number":6}', 0),
	(53, 'asdasd', '9', 10, 2004, '{"number":0}', 0),
	(54, 'ddddd', '341', 1, 2012, '{"lang":"English","orig_lang":"Polish"}', 0),
	(55, 'aaaaa', '311', 0, 1993, '{"lang":"English","orig_lang":"English"}', 0),
	(56, 'aaasd', '213', 8, 2001, '{"number":10}', 0),
	(57, 'dasd', '96', 4, 2001, '{"number":0}', 0),
	(58, '123123', '33', 2, 1997, '{"lang":"Romanian","orig_lang":"Polish"}', 0),
	(59, '123123', '395', 4, 2013, '{"lang":"Polish","orig_lang":"Polish"}', 0),
	(60, 'aaasd', '173', 0, 2010, '{"number":4}', 0),
	(61, '1231231111', '103', 0, 1992, '{"lang":"Polish","orig_lang":"Romanian"}', 0),
	(62, '111', '388', 8, 2019, '{"lang":"English","orig_lang":"Romanian"}', 0),
	(63, 'asdasdasaaa', '37x39', 8, 1993, '[]', 0),
	(64, '1111', '57', 8, 2005, '{"lang":"Polish","orig_lang":"English"}', 0),
	(65, '3333', '277', 10, 2006, '{"lang":"English","orig_lang":"Romanian"}', 0),
	(66, '123', '257', 10, 2017, '{"lang":"Romanian","orig_lang":"English"}', 0),
	(67, '123', '35', 1, 1996, '{"lang":"Romanian","orig_lang":"Romanian"}', 0),
	(68, '123123', '368', 6, 2016, '{"number":1}', 0),
	(69, '3313', '0', 4, 2006, '[]', 0),
	(70, '333', '0', 1, 2010, '[]', 0),
	(71, '345', '144', 1, 2008, '{"lang":"English","orig_lang":"Romanian"}', 0),
	(72, '123', '195', 9, 1995, '{"lang":"Romanian","orig_lang":"Polish"}', 0),
	(73, '123', '394', 9, 2004, '{"lang":"Romanian","orig_lang":"English"}', 0),
	(74, '345', '179', 3, 2011, '{"number":8}', 0),
	(75, '345', '208', 10, 1990, '{"number":2}', 0),
	(76, '345', '251', 1, 2010, '{"number":4}', 0),
	(77, '8790', '117', 9, 2005, '{"number":7}', 0),
	(78, '8790', '104', 1, 2019, '{"number":2}', 0),
	(79, '8790', '40', 7, 2011, '{"number":0}', 0),
	(80, '123', '47', 7, 1991, '{"lang":"English","orig_lang":"Romanian"}', 0),
	(81, '123', '18', 7, 2019, '{"lang":"English","orig_lang":"Polish"}', 0),
	(82, '333', '214', 10, 2007, '{"number":5}', 0),
	(83, '333', '188', 8, 1997, '{"number":3}', 0),
	(84, 'aaasd', '0', 2, 2015, '[]', 1);
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;

-- Zrzut struktury tabela recruitmentprojectv1.statistics
CREATE TABLE IF NOT EXISTS `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_elements` int(11) NOT NULL DEFAULT '0',
  `copies` int(11) NOT NULL DEFAULT '0',
  `books` int(11) NOT NULL DEFAULT '0',
  `newspapers` int(11) NOT NULL DEFAULT '0',
  `posters` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Zrzucanie danych dla tabeli recruitmentprojectv1.statistics: ~18 rows (około)
/*!40000 ALTER TABLE `statistics` DISABLE KEYS */;
INSERT INTO `statistics` (`id`, `unique_elements`, `copies`, `books`, `newspapers`, `posters`, `active`) VALUES
	(1, 11, 21, 31, 2, 1, 0),
	(2, 0, 0, 1, 0, 0, 0),
	(3, 0, 0, 2, 1, 1, 0),
	(4, 0, 0, 2, 1, 1, 0),
	(5, 0, 0, 2, 2, 1, 0),
	(6, 3, 42, 2, 5, 1, 0),
	(7, 3, 49, 2, 5, 1, 0),
	(8, 0, 0, 1, 1, 1, 0),
	(9, 0, 0, 1, 1, 1, 0),
	(10, 0, 0, 1, 1, 1, 0),
	(11, 0, 0, 0, 0, 0, 0),
	(12, 0, 0, 0, 0, 0, 0),
	(13, 1, 7, 1, 0, 0, 0),
	(14, 1, 14, 2, 0, 0, 0),
	(15, 2, 24, 2, 1, 0, 0),
	(16, 2, 32, 2, 2, 0, 0),
	(17, 0, 0, 0, 0, 0, 0),
	(18, 1, 2, 0, 0, 1, 1);
/*!40000 ALTER TABLE `statistics` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
