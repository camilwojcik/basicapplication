<?php
//DIRECTORIES
define('ROOT', dirname(__DIR__, 2) . DIRECTORY_SEPARATOR);
define('VENDOR', ROOT . 'vendor'. DIRECTORY_SEPARATOR);
define('APP', ROOT . 'application' . DIRECTORY_SEPARATOR);
define('CONFIG', APP . 'config'. DIRECTORY_SEPARATOR);
define('VIEW', ROOT . 'application' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR);
define('MODEL', ROOT . 'application' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR);
define('CORE', ROOT . 'application' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR);
define('CONTROLLER', ROOT . 'application' . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR);
define('LAYOUT', VIEW . 'default' . DIRECTORY_SEPARATOR);
define('STYLESHEETS', ROOT . 'public' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR);

//ELEMENT TYPES
define('ELEMENT_BOOK', 'book');
define('ELEMENT_NEWSPAPER', 'newspaper');
define('ELEMENT_POSTER', 'poster');

define('ELEMENTS', array(ELEMENT_BOOK, ELEMENT_NEWSPAPER, ELEMENT_POSTER));

//ADDITIONAL INFO FIELDS
define('ADDITIONAL_INFO_ORIGINAL_LANGUAGE', 'orig_lang');
define('ADDITIONAL_INFO_LANGUAGE', 'lang');
define('ADDITIONAL_INFO_NUMBER', 'number');

//RANGE OF RANDOM VALUES
define('MIN_NUMBER', 0);
define('MAX_NUMBER', 10);

define('MIN_COPIES', 0);
define('MAX_COPIES', 10);

define('MIN_PAGE_COUNT', 1);
define('MAX_PAGE_COUNT', 399);

define('MIN_YEAR', 1990);

define('MIN_POSTER_HEIGHT', 10);
define('MAX_POSTER_HEIGHT', 100);

define('MIN_POSTER_WIDTH', 10);
define('MAX_POSTER_WIDTH', 60);
