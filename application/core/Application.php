<?php


class Application
{
    protected $controller = 'homeController';
    protected $action = 'indexAction';
    protected $params = [];

    /**Calls methods with given parameters if they`re expected
     * Application constructor.
     */
    public function __construct()
    {
        $this->parseUrl();

        if(file_exists(CONTROLLER . $this->controller . '.php')){
            $this->controller = new $this->controller;
            if(method_exists($this->controller, $this->action)){
                call_user_func_array([$this->controller, $this->action], $this->params);
            }
        }
    }

    /**
     * parses given url and sets fields
     */
    public function parseUrl() : void
    {
        $request = trim($_SERVER['REQUEST_URI'], '/');
        if(!empty($request)){
            $url = explode('/', $request);

            $this->controller = isset($url[0]) ? $url[0].'Controller' : 'homeController';
            $this->action = isset($url[1]) ? $url[1].'Action' : 'indexAction';

            unset($url[0], $url[1]);
            $this->params = !empty($url) ? array_values($url) : [];
            $this->params = array_merge($this->params, $_POST);
        }
    }
}