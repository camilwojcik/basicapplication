<?php


class BaseModel
{
    public $_db;

    protected function __construct()
    {
        $this->_db = DatabaseConnection::getInstance();
    }

    /**Returns array of values from a dictionary based on given key
     * @param string $dictionary_key
     * @return array - values of desired dictionary | empty array when invalid dictionary name
     */
    public function getDictionaryValues(string $dictionary_key) : array
    {
        $dictionary_values = [];
        $sql = $this->_db->prepare("
            SELECT * FROM dictionaries_values AS dv 
            LEFT JOIN dictionaries d on dv.dictionary_id = d.id
            WHERE d.`key`= :key AND dv.active=1
        ");
        $sql->execute(['key'=>$dictionary_key]);
        while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
            $dictionary_values[] = array(
                'id'    => $row['id'],
                'value' => $row['value']
            );
        }
        unset($sql);
        return $dictionary_values;
    }

    /**Adds a value to desired dictionary
     * @param string $dictionary_name - key from `dictionaries`
     * @param $value - value to add into dictionary
     * @return int - id of added value || 0 on failure
     */
    private function addDictionaryValue(string $dictionary_name, string $value) : int
    {
        $dictionary_id = $this->getDictionaryIdByKey($dictionary_name);
        $sql = "INSERT INTO dictionaries_values (value, dictionary_id) VALUES (:value, :dictionary_id)";
        $this->_db->prepare($sql)->execute(array('value'=>$value, 'dictionary_id' => $dictionary_id));
        return $this->checkExistingValue($dictionary_id, $value);
    }

    /**Adds dictionary, returns it`s id if exists
     * @param string $dictionary_name - key from `dictionaries`
     * @param string $dictionary_key
     * @return int - created/existing dictionary id
     */
    private function createDictionary(string $dictionary_name, string $dictionary_key) : int
    {
        $exists = $this->getDictionaryIdByKey($dictionary_key);
        if(!empty($exists) && $exists!=0)
            return $exists;

        $sql = "INSERT INTO  dictionaries (name, `key`) VALUES ($dictionary_name, $dictionary_key)";
        $this->_db->prepare($sql)->execute([]);
        return $this->getDictionaryIdByKey($dictionary_key);
    }

    /**selects dictionaries and returns id of given dictionary
     * @param string $dictionary_key
     * @return int
     */
    private function getDictionaryIdByKey(string $dictionary_key): int
    {
        $sql = $this->_db->prepare("
            SELECT * FROM dictionaries AS d
            WHERE d.`key`= :key
        ");
        $sql->execute(['key'=>$dictionary_key]);
        return $sql->fetch(PDO::FETCH_ASSOC)['id'] ?? 0;
    }

    /**Searches for desired active value in a dictionary
     * @param int $dictionary_id
     * @param string $value
     * @return int
     */
    private function checkExistingValue(int $dictionary_id, string $value): int
    {
        $sql = $this->_db->prepare("
            SELECT * FROM dictionaries_values AS dv
            WHERE dv.dictionary_id= :key AND dv.value= :value AND dv.active=1
        ");
        $sql->execute(['key'=>$dictionary_id, 'value'=>$value]);
        return $sql->fetch(PDO::FETCH_ASSOC)['id'] ?? 0;
    }
}