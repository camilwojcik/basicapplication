<?php

class DatabaseConnection
{
    protected static $host = 'localhost';
    protected static $dbName = 'recruitmentprojectv1';
    protected static $userName = 'root';
    protected static $password = '';
    protected static $collation= 'utf8';

    /**
     * @var Singleton
     */
    private static $instance;


    /**
     * gets the instance via lazy initialization (created on first usage)
     * @return PDO|Singleton
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = self::connect();
        }

        return static::$instance;
    }

    /**Default DB connection
     * @return PDO
     */
    public static function connect() : PDO
    {
        $dsn = "mysql:host=".self::$host.";dbname=".self::$dbName.";charset=".self::$collation.";";
        $pdo = new PDO($dsn, self::$userName, self::$password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }
}