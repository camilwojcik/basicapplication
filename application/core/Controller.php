<?php


/**Base controller, all controllers are meant to extend this class in order to generate view and parse some data into them
 * Class Controller
 * @author CamilWojcik <camilwojcik@gmail.pl>
 */
class Controller
{
    protected $view;

    /**Generates view object and stores it in a view field
     * @param string $viewName
     * @param array|null $view_data
     * @return View
     */
    public function view(string $viewName, array $view_data = null)
    {
        $this->view = new View($viewName, $view_data);
        return $this->view;
    }

    public function getSourcePath($_CLASS, $_METHOD){
        return $this->getControllerName($_CLASS) . '/' . $this->getMethodName($_METHOD);
    }

    public function getControllerName(string $_CLASS){
        return substr($_CLASS, 0, -10);
    }

    public function getMethodName(string $_METHOD){
        return substr($_METHOD, 0, -6);
    }
}