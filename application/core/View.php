<?php


class View
{
    protected $view_file;
    protected $view_data = [];

    /**
     * View constructor.
     * @param string $view_file
     * @param array|null $view_data
     */
    public function __construct(string $view_file, array $view_data = null)
    {
        $this->view_file = $view_file;
        $this->view_data = $view_data;
    }

    /**Includes site views
     *
     */
    public function renderView():void
    {
        if(file_exists(VIEW . $this->view_file . '.phtml')){
            include LAYOUT . 'header.phtml';
            include VIEW . $this->view_file . '.phtml';
            include LAYOUT . 'footer.phtml';
        }
    }

    public function getAction(): string {
        return explode('/' ,$this->view_file)[1];
    }
}