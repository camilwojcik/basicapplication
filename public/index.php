<?php
require __DIR__ . '/../application/config/constants.php';
$modules = [ROOT, APP, CORE, CONTROLLER, MODEL, CONFIG];

set_include_path(get_include_path() . PATH_SEPARATOR . implode(PATH_SEPARATOR, $modules));
spl_autoload_register('spl_autoload', false);
$app = new Application();
